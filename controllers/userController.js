const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then( result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
};

/*
BUSINESS LOGIC
Create a new User object using the mongoose model and the information from the request body
Make sure that the password is encrypted
Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	});
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	});
};

/*
Business Logic:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend
*/

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

//Authenticated user enrollment
/*
Business Logic:
Find the document in the database using the user's ID
Add the course ID to the user's enrollment array
Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
 		});
 	});

 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
 			course.enrolees.push({userId: data.userId});
 		return course.save().then((user, err) => {
			if(err) {
				return false;
			} else {
				return true;
			}
 		});
 	});

 		if(isUserUpdated && isCourseUpdated) {
 			return true;
 		} else {
 			return false;
 		}
};

