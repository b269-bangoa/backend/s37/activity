const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoute);
app.use("/courses", courseRoute);

mongoose.connect ("mongodb+srv://yanab:admin123@zuitt-bottcamp.muceymo.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log(`Now connected to cloud database`));


app.listen(process.env.PORT || 3004, () => console.log(`Now connected to ${process.env.PORT||3004}`));