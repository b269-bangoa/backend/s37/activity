const jwt = require("jsonwebtoken");

/*
Anatomy:
Header - type of token and signing algorithm
Payload - contains the claims (id, email, isAdmin)
Signature - generate by putting the encoded header, the enencoded payload and applying the algorithm in the header
*/

const secret = "CourseBookingAPI";


//Token Creation
module.exports.createAccessToken = (user) => {
	//payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};


// Token verification

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: 'failed'});
			} else {
				//used as 'token' for next func
				next();
			}
		})
	} else {
		return res.send({auth: "failed"});
	};
};

module.exports.decode = (token) => {
	if(typeof token !== "undefinded"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null;
			} else {
				return jwt.decode(token, { complete: true}).payload
				//next();
			};
		})
	} else {
		return null;
	};
};
